import { Component, OnInit } from '@angular/core';
import { DataSnapshot } from "@angular/fire/database/interfaces";
import { Router } from "@angular/router";
import { AngularFireDatabase } from 'angularfire2/database'
import { Casefile } from "src/app/model/casefile.model";

@Component({
  selector: 'app-casefile-list',
  templateUrl: './casefile-list.component.html',
  styleUrls: ['./casefile-list.component.css']
})
export class CasefileListComponent implements OnInit {

  public casefiles: Casefile[];
  public filteredCasefiles: Casefile[];
  public searchTerm = "";
  public selectedActiveFilterOption;
  public readOnly;
  public activeCasefilesNumber;

  // public page = 1;
  // private lastId = "";

  public filterActiveOptions = [
    {
      value: undefined,
      text: "Casefile.CasefileListComponent.show-all"
    }, {
      value: true,
      text:"Casefile.CasefileListComponent.active"
    }, {
      value: false,
      text: "Casefile.CasefileListComponent.inactive"
    }
  ];

  constructor(
    private readonly db: AngularFireDatabase,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
      this.db.list("casefile").valueChanges().subscribe((casefiles: Casefile[]) => {
        this.casefiles = casefiles;
        this.filteredCasefiles = casefiles;
      });

    this.db.object("readonly").valueChanges().subscribe((readOnly: boolean) => this.readOnly = readOnly);
    this.sort();
  }

  public editCasefile(casefileId: string) {
    this.router.navigate([`updatecasefile/${casefileId}`]);
  }

  public deleteCasefile(casefileId: string) {
    this.db.list("casefile").remove(casefileId);
  }

  public selectCasefile(casefileId: string) {
    this.router.navigate([`casefiledetails/${casefileId}`]);
  }

  public addCasefile() {
    this.router.navigate(["addcasefile"]);
  }

  public searchCasefile() {
    this.filterByFirstNameAndLastName();
  }

  public onChangeActiveFilter() {
    if (this.selectedActiveFilterOption === undefined) {
      this.db.list("casefile").valueChanges().subscribe((casefiles: Casefile[]) => {
        this.casefiles = casefiles;
        this.filteredCasefiles = casefiles;
        this.filterByFirstNameAndLastName();
      });
    }
    else {
      this.db.database.ref().child("casefile").orderByChild("active").equalTo(this.selectedActiveFilterOption).once("value").then(dataSnapshot => {
        this.initializeCasefiles(dataSnapshot);
        this.filterByFirstNameAndLastName();
      });
    }
  }

  private filterByFirstNameAndLastName() {
    this.filteredCasefiles = this.casefiles.filter(casefile => casefile.patient.firstName.toLowerCase().includes(this.searchTerm.toLowerCase()) || 
                                                               casefile.patient.lastName.toLowerCase().includes(this.searchTerm.toLowerCase()));
  }

  public onChangeReadOnly() {
    const child = this.db.database.ref().child("readonly");
    child.set(this.readOnly);
  }

  public activateCasefile(casefile: Casefile) {
    casefile.active = !casefile.active;
    const child = this.db.database.ref().child("casefile");
    child.child(casefile.id).set(casefile);
  }

  public calculateNumberActiveCasefiles(): number {
    if (this.filteredCasefiles) {
      return this.filteredCasefiles.filter(casefile => casefile.active).length;
    }
    return 0;
  }

  private initializeCasefiles(dataSnapshot: DataSnapshot) {
    this.casefiles = [];
    dataSnapshot.forEach(casefile => {this.casefiles.push(casefile.val())});
    this.filteredCasefiles = this.casefiles;
  }

  public sort() {
    this.db.database.ref().child("casefile").orderByChild("caseCode").once("value").then(dataSnapshot => {
      this.initializeCasefiles(dataSnapshot);
      this.filterByFirstNameAndLastName();
    });
  }

    // public previousPage() {
  //   this.page -= 1;
  //   this.db.database.ref().child("casefile").orderByChild("id").endAt(this.lastId).limitToFirst(this.page * 5).once("value").then(dataSnapshot => {
  //     this.initializeCasefiles(dataSnapshot);
  //     this.lastId = this.filteredCasefiles[this.filteredCasefiles.length - 1].id;
  //   });
  // }

  // public nextPage() {
  //   this.page += 1;
  //   this.db.database.ref().child("casefile").orderByChild("id").startAt(this.lastId).limitToFirst(this.page * 5).once("value").then(dataSnapshot => {
  //     this.initializeCasefiles(dataSnapshot);
  //     this.lastId = this.filteredCasefiles[this.filteredCasefiles.length - 1].id;
  //   });
  // }
}
