import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CasefileListComponent } from './casefile-list.component';

describe('CasefileListComponent', () => {
  let component: CasefileListComponent;
  let fixture: ComponentFixture<CasefileListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CasefileListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CasefileListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
