import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCasefileComponent } from './add-casefile.component';

describe('AddCasefileComponent', () => {
  let component: AddCasefileComponent;
  let fixture: ComponentFixture<AddCasefileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCasefileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCasefileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
