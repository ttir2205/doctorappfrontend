import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { AngularFireDatabase } from 'angularfire2/database'
import { Casefile } from "src/app/model/casefile.model";
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-casefile',
  templateUrl: './add-casefile.component.html',
  styleUrls: ['./add-casefile.component.css']
})
export class AddCasefileComponent implements OnInit {

  public addCasefileForm: FormGroup;

  constructor(
    private db: AngularFireDatabase,
    private router: Router,
    private translateService: TranslateService,
    private snackBar: MatSnackBar)
     { }

  ngOnInit(): void {
    this.addCasefileForm = new FormGroup({
      caseCode: new FormControl("", [Validators.pattern("[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}$")]),
      description: new FormControl("", [Validators.required]),
      firstName: new FormControl("", [Validators.required]),
      lastName: new FormControl("", [Validators.required]),
      gender: new FormControl("Male"),
      birthdate: new FormControl("", [Validators.required])
    });
  }

  public addCasefile(addCasefileFormValue) {
    let casefile : Casefile = {
      caseCode: addCasefileFormValue.caseCode,
      active: true,
      description: addCasefileFormValue.description,
      patient : {
        firstName: addCasefileFormValue.firstName,
        lastName: addCasefileFormValue.lastName,
        birthdate: addCasefileFormValue.birthdate,
        gender: addCasefileFormValue.gender
      }
    }
    if (!this.addCasefileForm.valid) {
      this.snackBar.open(this.translateService.instant("Casefile.AddCasefileComponent.error-form"), "", {duration: 3000, panelClass: "custom-sneak-bar" });
    } else {
      const child = this.db.database.ref().child("casefile");
      casefile.id = child.push().key;
      child.child(casefile.id).set(casefile);
      this.router.navigate([""]);
    }
  }



}
