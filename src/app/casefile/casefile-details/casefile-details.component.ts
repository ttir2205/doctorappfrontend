import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { ActivatedRoute } from "@angular/router";
import { Casefile } from "src/app/model/casefile.model";

@Component({
  selector: 'app-casefile-details',
  templateUrl: './casefile-details.component.html',
  styleUrls: ['./casefile-details.component.css']
})
export class CasefileDetailsComponent implements OnInit {

  public casefile: Casefile;

  constructor( 
    private readonly db: AngularFireDatabase,
    private readonly activatedRoute : ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    this.db.object(`casefile/${id}`).valueChanges().subscribe((casefile : Casefile) => {
      this.casefile = casefile;
    });
  }

  public getGenderTranslated(gender: String): string {
    switch (gender) {
      case "Male":
        return "Casefile.CasefileDetailsComponent.male";
      case "Female":
        return "Casefile.CasefileDetailsComponent.female";
    }
  }

}
