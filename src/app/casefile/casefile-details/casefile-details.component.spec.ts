import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CasefileDetailsComponent } from './casefile-details.component';

describe('CasefileDetailsComponent', () => {
  let component: CasefileDetailsComponent;
  let fixture: ComponentFixture<CasefileDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CasefileDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CasefileDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
