import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Casefile } from "src/app/model/casefile.model";
import { AngularFireDatabase } from 'angularfire2/database'
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-update-casefile',
  templateUrl: './update-casefile.component.html',
  styleUrls: ['./update-casefile.component.css']
})
export class UpdateCasefileComponent implements OnInit {
  
  public casefileToUpdate: Casefile;
  public updateCasefileForm: FormGroup;

  constructor(
    private readonly db: AngularFireDatabase,
    private readonly activatedRoute : ActivatedRoute,
    private readonly router: Router,
    private translateService: TranslateService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    this.updateCasefileForm = new FormGroup({
      caseCode: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
      firstName: new FormControl("", [Validators.required]),
      lastName: new FormControl("", [Validators.required]),
      gender: new FormControl(""),
      birthdate: new FormControl("", [Validators.required])
    });
    this.db.object(`casefile/${id}`).valueChanges().subscribe((casefile : Casefile) => {
      this.casefileToUpdate = casefile;
      this.updateCasefileForm.patchValue(this.casefileToUpdate);
      this.updateCasefileForm.patchValue(this.casefileToUpdate.patient);
    });

  }

  public updateCasefile(updateCasefileFormValue) {
    let casefile : Casefile = {
      caseCode: updateCasefileFormValue.caseCode,
      active: this.casefileToUpdate.active,
      description: updateCasefileFormValue.description,
      patient : {
        firstName: updateCasefileFormValue.firstName,
        lastName: updateCasefileFormValue.lastName,
        birthdate: updateCasefileFormValue.birthdate,
        gender: updateCasefileFormValue.gender
      }
    }
    if (!this.updateCasefileForm.valid) {
      this.snackBar.open(this.translateService.instant("Casefile.UpdateCasefileComponent.error-form"), "", {duration: 3000, panelClass: "custom-sneak-bar" });
    } else {
      casefile.id = this.casefileToUpdate.id;
      const child = this.db.database.ref().child("casefile");
      child.child(casefile.id).set(casefile);
      this.router.navigate([""]);
    }
  }



}
