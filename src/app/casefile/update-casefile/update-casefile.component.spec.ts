import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCasefileComponent } from './update-casefile.component';

describe('UpdateCasefileComponent', () => {
  let component: UpdateCasefileComponent;
  let fixture: ComponentFixture<UpdateCasefileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateCasefileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCasefileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
