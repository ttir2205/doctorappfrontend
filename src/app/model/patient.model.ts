export interface Patient {
    firstName: string;
    lastName: string;
    gender: string;
    birthdate: string;
}