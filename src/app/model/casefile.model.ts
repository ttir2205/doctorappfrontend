import { Patient } from "./patient.model";

export interface Casefile {
    id?: string;
    caseCode: string;
    active: boolean;
    description: string;
    patient: Patient;
}