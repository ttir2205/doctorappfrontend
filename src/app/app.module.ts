import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CasefileListComponent } from './casefile/casefile-list/casefile-list.component';
import { AngularFireModule } from "@angular/fire";
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AddCasefileComponent } from './/casefile/add-casefile/add-casefile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateCasefileComponent } from "./casefile/update-casefile/update-casefile.component";
import { CasefileDetailsComponent } from './casefile/casefile-details/casefile-details.component';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSnackBarModule} from '@angular/material/snack-bar';


export const firebaseConfig = {
  apiKey: "AIzaSyDa-Bs-ufLCmVeVSlZrHC0A2PlD2btLUqI",
  authDomain: "doctorapp-17767.firebaseapp.com",
  projectId: "doctorapp-17767",
  storageBucket: "doctorapp-17767.appspot.com",
  messagingSenderId: "61399247612",
  appId: "1:61399247612:web:e448d692d40e81101c4579",
  measurementId: "G-FPWB953P8P",
  databaseURL: "https://doctorapp-17767-default-rtdb.europe-west1.firebasedatabase.app"
};


@NgModule({
  declarations: [
    AppComponent,
    CasefileListComponent,
    AddCasefileComponent,
    UpdateCasefileComponent,
    CasefileDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig, "DoctorApp"),
    AngularFireDatabaseModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    HttpClientModule,
    MatSnackBarModule,
    TranslateModule.forRoot({
      loader:{
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http);
}

