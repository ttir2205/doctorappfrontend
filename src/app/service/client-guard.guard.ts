import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
  })

export class ClientGuard implements CanActivate {

    constructor(private readonly db: AngularFireDatabase) {}

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> {
          return this.db.object("readonly").valueChanges().pipe(map(value => !value));
    }
}