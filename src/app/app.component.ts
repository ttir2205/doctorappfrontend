import { Component } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public selectedLanguage = "en";

  constructor(private translateService: TranslateService){
    translateService.addLangs(['en','ro']); 
    translateService.setDefaultLang('en');
  }

  onChangeLanguage(language: string) {
      this.translateService.use(language);
  }

}
