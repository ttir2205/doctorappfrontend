import { CommonModule } from "@angular/common";
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddCasefileComponent } from "./casefile/add-casefile/add-casefile.component";
import { CasefileDetailsComponent } from "./casefile/casefile-details/casefile-details.component";
import { CasefileListComponent } from './casefile/casefile-list/casefile-list.component';
import { UpdateCasefileComponent } from "./casefile/update-casefile/update-casefile.component";
import { ClientGuard } from "./service/client-guard.guard";

const routes: Routes = [
  { path: '', component: CasefileListComponent },
  { path: 'addcasefile', component: AddCasefileComponent, canActivate: [ClientGuard] },
  { path: 'updatecasefile/:id', component: UpdateCasefileComponent, canActivate: [ClientGuard] },
  { path: 'casefiledetails/:id', component: CasefileDetailsComponent, canActivate: [ClientGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
